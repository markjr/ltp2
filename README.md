# Bem-vindo ao Curso

### Engenharia da Computação

#### Disciplina: **Linguagem e Técnicas de Programação II**

#### Código da turma: **k6df5ak**

#### Professor: Professor MSc. Ricardo José Menezes Maia.  http://lattes.cnpq.br/0706885145380777
* Doutorado, Universidade de Brasília UNB - Ciência da Computação - 2019 - em andamento
* Mestrado, Universidade de São Paulo USP - Escola Politécnica, Engenharia de Computação - 2008 a 2010
* Bacharelado em Ciência da Computação, Universidade Federal do Amazonas - 1999 a 2003
* Técnico em Programação, Fundação de Ensino e Pesquisa Matias Machline - Sharp do Brasil - 1992 a 1994.

Desejo que nossa experiência neste curso seja a melhor possível!

Abaixo está o sumário do curso e para baixar todo conteúdo que está versionado no git clique no link
https://gitlab.com/ricardo.jmm/ltp2/-/archive/master/ltp2-master.zip

Após isso descompacte o arquivo em uma pasta da sua escolha


Plano de Ensino está no arquivo PlanoEnsinoLTP_II_prof_RicardoMaia.docx
[Plano de Ensino](PlanoEnsinoLTP_II_prof_RicardoMaia.docx)

* A disciplina possuirá 3 avaliações:

* * Primeira avaliação data provável: 08/10/2020

* * Segunda avaliação data provável: 06/11/2020

* * Terceira avaliação data provável: 04/12/2020


Link das aulas mediadas pelo google meet:

* 21/08/2020 - https://meet.google.com/wuu-bsjy-iku 
* * Gravação - https://drive.google.com/file/d/1gvDNFnPWGNo2GSnmy185NzS0Fr7ZjWM6/view?usp=sharing

* 28/08/2020 - https://meet.google.com/zgk-wyyb-zmd

* 04/09/2020 - https://meet.google.com/qhw-pacr-esf

* 10/09/2020 - https://meet.google.com/rqa-qsqs-dcm

* 11/09/2020 - https://meet.google.com/afp-ujaf-dno

* 17/09/2020 - https://meet.google.com/jep-heoj-vqa

* 18/09/2020 - https://meet.google.com/uon-ympc-mpo

* 24/09/2020 - https://meet.google.com/mwq-fvnp-kfi

* 25/09/2020 - https://meet.google.com/cfg-qzme-ztj

* 01/10/2020 - https://meet.google.com/kud-yout-ptk

* 02/10/2020 - https://meet.google.com/skb-gdxe-cfw

* 08/10/2020 - https://meet.google.com/dxn-hyfm-cgc

* 09/10/2020 - https://meet.google.com/zry-fupr-ieo

* 22/10/2020 - https://meet.google.com/rnj-xgdq-bqc

* 29/10/2020 - https://meet.google.com/aij-arrb-ept

* 30/10/2020 - https://meet.google.com/tci-tvmk-szz

* 05/11/2020 - https://meet.google.com/vop-exog-sai

* 12/11/2020 - https://meet.google.com/fhg-difu-iiu

* 13/11/2020 - https://meet.google.com/fec-cmiq-men

* 19/11/2020 - https://meet.google.com/onv-vhij-hrt

* 20/11/2020 - https://meet.google.com/kon-kitv-fcu

* 26/11/2020 - https://meet.google.com/hye-hvqk-ziz

* 27/11/2020 - https://meet.google.com/aao-vbki-myz

* 03/12/2020 - https://meet.google.com/tce-scho-uvp

* 04/12/2020 - https://meet.google.com/ttx-kwcm-qsn

* 10/12/2020 - https://meet.google.com/nyw-nmjb-rez

* 17/12/2020 - https://meet.google.com/wuk-rnpo-kxx


### Referências

##### Básica

DEITEL, Harvey. M; DEITEL, P. J. Java como programar. 8. ed. Porto Alegre: Bookman, 2007.

PUGA, Sandra; RISSETTI, Gérson. Lógica de programação e estruturas de dados com aplicações em Java. São Paulo: Prentice Hall, 2004.

SCHILDT, Herbert. Java para iniciantes: crie, compile e execute programas Java rapidamente. 5.ed. São Paulo: Bookman, 2013.

##### Complementar
HORSTMANN, Cay S.; GARY, Cornell. Core JAVA 2 fundamentos. São Paulo: Makron Books, 2004. v. 1. 

SIERRA, Kathy E BATES, Bert. Certificação sun para programador java 6. São Paulo: Alta Books, 2008.

SIERRA, Katy; BATES, Bert. Use a cabeça Java. 2. ed. Rio de Janeiro: Alta Books, 2006

SANTOS, Rafael. Introdução a programação orientada a objetos usando Java. Rio de Janeiro: Campus, 2003.

JANDL JÚNIOR, PETER. Introdução ao Java. São Paulo: Berkeley, 2002.

## IDE

https://www.eclipse.org/

https://www.eclipse.org/downloads/

https://www.java.com/pt_BR/

https://www.onlinegdb.com/online_java_compiler

https://code.visualstudio.com/

https://www.jetbrains.com/idea/

## Sumário:

* [01_PlanoEnsino](PlanoEnsinoLTP_II_ECAN_ECTG_prof_RicardoMaia.docx)
* [Por que todos deveriam aprender a programar?](https://youtu.be/mHW1Hsqlp6A)
* [Ranking da Linguagem Java](https://www.tiobe.com/tiobe-index/)
* [Estatística do Stackoverflow](https://stackoverflow.com/tags)
