/*
 * Esta será nossa primeira 'classe' em Java

	Vou transformar esta classe em um aplicativo 
	desktop
	https://onlinegdb.com/SJ-b5fD7v
 */

public class PrimeiroPrograma {
	 
	/*
	 * Vamos precisar criar um método especial
	 * para eu conseguir executar o programa em um 
	 * Desktop / PC
	 * Run as Java Application
	 */
	public static void main(String args[]) {
		System.out.println("Meu programa java!!");
	}
}
