// Importar classes
import java.util.Scanner; // Scanner est� dentro de um pacote chamado java.util

/*
public � uma palavra reservada - Java
class � uma palavra reservada - Java
*/

// Este � o programa da aula 02 - 22/09/2020

/**
 * Nome da classe que foi criada usando public class o nome do arquivo
 * precisa ser igual ao nome da classe.
 * A diferen�a � que o nome do arquivo tem extens�o java
 * @author Ricardo
 * @version 1.0
 * Esta classe poderia executar em um TV, celular, drone,
 * Cada dispositivo tem uma forma de executar
 * Criar um aplicativo desktop que roda em PC
 * 
 * Objetivo: Did�tico, Ler dois n�meros e calcular a m�dia e exibir
 * 
 */
public class Principal { // Come�a a classe

	// Vou criar um m�todo para executar um programa java
	// String � uma classe
	public static void main(String args[]) {
		// Isto aqui � para ler do teclado
		Scanner leitor = new Scanner(System.in); // new � para criar objeto
		double primeiro, segundo;
		double media;
		
		// imprimir uma mensagem na tela
		System.out.print("Entre com o primeiro numero: ");
		primeiro = leitor.nextDouble();

		// imprimir uma mensagem na tela
		System.out.print("Entre com o segundo numero: "); 
		segundo = leitor.nextDouble();
		
		media = (primeiro + segundo ) / 2.0;
		
		System.out.println("A media aritmetica eh  " + media);

		System.out.println("Finalizou");
	}

} // termina a classe
